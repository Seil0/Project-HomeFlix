/**
 * Project-HomeFlix
 *
 * Copyright 2016-2022  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

package org.mosad.homeflix.application.view;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mosad.homeflix.controller.DBController;
import org.mosad.homeflix.controller.XMLController;
import org.mosad.homeflix.datatypes.OMDbAPIResponseDataType;
import org.mosad.homeflix.player.Player;

import com.jfoenix.controls.JFXButton;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

public class FilmDetailView extends DetailView {
	
	@FXML private Label lblTitle;
	@FXML private Label lblYear;
	@FXML private Label lblScore;
	
	@FXML private Label lblCrew;
	@FXML private Label lblDirectors;
	@FXML private Label lblDirectorsInfo;
	@FXML private Label lblWriters;
	@FXML private Label lblWritersInfo;
	@FXML private Label lblActors;
	@FXML private Label lblActorsInfo;
	
	@FXML private Label lblInfo;
	@FXML private Label lblRuntimeInfo;
	@FXML private Label lblRuntime;
	@FXML private Label lblLanguageInfo;
	@FXML private Label lblLanguage;
	@FXML private Label lblRevenueInfo;
	@FXML private Label lblRevenue;
	@FXML private Label lblRatingInfo;
	@FXML private Label lblRating;
	
	@FXML private JFXButton btnWishlist;
	@FXML private JFXButton btnFavourite;
	@FXML private JFXButton btnHide;
	@FXML private JFXButton btnPlay;
	@FXML private JFXButton btnDirectory;
	
	@FXML private ImageView wishlistIcon;
	@FXML private ImageView favoriteIcon;
	@FXML private ImageView imgPoster;
	
	@FXML private Text textPlot;
	
	private DBController dbController;
	private static final Logger LOGGER = LogManager.getLogger(FilmDetailView.class.getName());
	private String currentStreamURL;
	
	public void initialize() {
		dbController = DBController.getInstance();
		rootPane.setStyle("-fx-background-color: rgba(89,89,89,0.9);");
	}
	
	@FXML
	private void btnWishlistAction() {
		
	}
	
	@FXML
	private void btnFavouriteAction() {
		dbController.toggleFavoriteState(currentStreamURL);
		
		// update the favorite icon
		if(dbController.getFavoriteState(currentStreamURL) == 1) {
			favoriteIcon.setImage(new Image("icons/baseline_favorite_black_48dp.png"));
		} else {
			favoriteIcon.setImage(new Image("icons/baseline_favorite_border_black_48dp.png"));
		}
	}
	
	@FXML
	private void btnHideAction() {
		hidePane();
	}
	
	@FXML
	private void btnPlayAction() {
		new Player(currentStreamURL);
	}
	
	@FXML
	private void btnDirectoryAction() {
		File dest = new File(currentStreamURL).getParentFile();

		if (!System.getProperty("os.name").contains("Linux")) {
			try {
				Desktop.getDesktop().open(dest);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * set the cached data of a stream to the FilmDetailView
	 * @param streamURL URL of the stream
	 */
	public void setFilm(String streamURL) {
		currentStreamURL = streamURL;
		OMDbAPIResponseDataType cacheInfo = dbController.readCache(streamURL); // get the cache data from the database
		
		// add the cache data to the GUI
		lblTitle.setText(cacheInfo.getTitle());
		lblYear.setText("(" + cacheInfo.getYear() + ")");
		lblScore.setText(XMLController.getLocalBundle().getString("score") + ": " + cacheInfo.getMetascore() + "%");
		
		textPlot.setText(cacheInfo.getPlot());
		
		lblDirectors.setText(cacheInfo.getDirector());
		lblWriters.setText(cacheInfo.getWriter());
		lblActors.setText(cacheInfo.getActors());
		
		lblRuntime.setText(cacheInfo.getRuntime());
		lblLanguage.setText(cacheInfo.getLanguage());
		lblRevenue.setText(cacheInfo.getBoxOffice());
		lblRating.setText(cacheInfo.getRated());
		
		try {
			if (new File(cacheInfo.getPoster()).isFile()) {
				imgPoster.setImage(new Image(new File(cacheInfo.getPoster()).toURI().toString()));
			} else {
				imgPoster.setImage(new Image(cacheInfo.getPoster()));
			}
		} catch (Exception e) {
			imgPoster.setImage(new Image("icons/Homeflix_Poster.png"));
			LOGGER.error("No Poster found, useing default.");
		}
		
		// set the favorite correct icon
		if(dbController.getFavoriteState(streamURL) == 1) {
			favoriteIcon.setImage(new Image("icons/baseline_favorite_black_48dp.png"));
		} else {
			favoriteIcon.setImage(new Image("icons/baseline_favorite_border_black_48dp.png"));
		}
		
	}
	
	/**
	 * update the text of all static GUI elements of FilmDeatilView
	 */
	@Override
	public void updateGUILocal() {
		lblCrew.setText(XMLController.getLocalBundle().getString("crew"));
		lblDirectorsInfo.setText(XMLController.getLocalBundle().getString("directors"));
		lblWritersInfo.setText(XMLController.getLocalBundle().getString("writers"));
		lblActorsInfo.setText(XMLController.getLocalBundle().getString("actors"));
		
		lblInfo.setText(XMLController.getLocalBundle().getString("info"));
		lblRuntimeInfo.setText(XMLController.getLocalBundle().getString("runtime"));
		lblLanguageInfo.setText(XMLController.getLocalBundle().getString("language"));
		lblRevenueInfo.setText(XMLController.getLocalBundle().getString("boxOffice"));
		lblRatingInfo.setText(XMLController.getLocalBundle().getString("rated"));
	}

}
