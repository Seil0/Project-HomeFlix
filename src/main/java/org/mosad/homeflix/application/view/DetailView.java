package org.mosad.homeflix.application.view;

import org.mosad.homeflix.application.MainWindowController;

import javafx.animation.FadeTransition;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class DetailView {
	
	@FXML protected AnchorPane rootPane;
	
	public void updateGUILocal() {
		throw new UnsupportedOperationException();
	}
	/**
	 * show the root pane
	 */
	public void showPane() {
		rootPane.setVisible(true);
		FadeTransition fadeIn = new FadeTransition(Duration.millis(300), rootPane);
		fadeIn.setFromValue(0.3);
		fadeIn.setToValue(1.0);
		fadeIn.play();
	}
	
	/**
	 * hide the root pane and disable the blur in MainWindowController
	 */
	protected void hidePane() {
		FadeTransition fadeOut = new FadeTransition(Duration.millis(200), rootPane);
		fadeOut.setFromValue(1.0);
		fadeOut.setToValue(0.3);
		fadeOut.play();

		rootPane.setVisible(false);
		
		MainWindowController.getInstance().disableBlur(); // disable blur
	}
	
}
