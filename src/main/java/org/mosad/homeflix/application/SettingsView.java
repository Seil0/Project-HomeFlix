/**
 * Project-HomeFlix
 *
 * Copyright 2016-2022  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

package org.mosad.homeflix.application;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mosad.homeflix.application.view.FilmDetailView;
import org.mosad.homeflix.controller.DBController;
import org.mosad.homeflix.controller.UpdateController;
import org.mosad.homeflix.controller.XMLController;
import org.mosad.homeflix.datatypes.SourceDataType;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXColorPicker;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXToggleButton;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

public class SettingsView {

    @FXML private ScrollPane settingsScrollPane;
    @FXML private AnchorPane settingsAnchorPane;

    @FXML private Label homeflixSettingsLbl;
    @FXML private Label mainColorLbl;
    @FXML private Label fontsizeLbl;
    @FXML private Label languageLbl;
    @FXML private Label updateLbl;
    @FXML private Label branchLbl;
    @FXML private Label versionLbl;
    @FXML private Label PlayerLbl;
    @FXML private Label sourcesLbl;
    
    @FXML private JFXColorPicker colorPicker;
    @FXML private JFXSlider fontsizeSlider;

    @FXML private ChoiceBox<String> languageChoisBox;
    @FXML private ChoiceBox<String> branchChoisBox;

    @FXML private JFXButton updateBtn;
    @FXML private JFXButton addStreamSourceBtn;
    @FXML private JFXButton addDirectoryBtn;

    @FXML private JFXToggleButton autoUpdateToggleBtn;
    @FXML private JFXToggleButton autoplayToggleBtn;
    @FXML private JFXToggleButton fullscreenToggleBtn;

    @FXML private TableView<SourceDataType> sourcesTable;
    @FXML private TableColumn<SourceDataType, String> sourceColumn;
    @FXML private TableColumn<SourceDataType, String> modeColumn;
    
    private XMLController xmlController;
    private static final Logger LOGGER = LogManager.getLogger(FilmDetailView.class.getName());
	private ObservableList<String> languages = FXCollections.observableArrayList("English (en_US)", "Deutsch (de_DE)");
	private ObservableList<String> branches = FXCollections.observableArrayList("stable", "beta");
	private static ObservableList<SourceDataType> sourcesList = FXCollections.observableArrayList();
    
    public void initialize() {
    	xmlController = new XMLController();
    	
    	// initialize the GUI elements
    	settingsScrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
    	versionLbl.setText("Version: " + Main.version + " (Build: " + Main.buildNumber + ")");
		fontsizeSlider.setValue(XMLController.getFontSize());
		colorPicker.setValue(Color.valueOf(XMLController.getColor()));

		autoUpdateToggleBtn.setSelected(XMLController.isAutoUpdate());
		autoplayToggleBtn.setSelected(XMLController.isAutoplay());
		fullscreenToggleBtn.setSelected(XMLController.isFullscreen());
		languageChoisBox.setItems(languages);
		branchChoisBox.setItems(branches);
		
		branchChoisBox.getSelectionModel().select(XMLController.isUseBeta() ? 1 : 0);
		// TODO switch expressions
		switch (XMLController.getUsrLocal()) {
		case "en_US":
			languageChoisBox.getSelectionModel().select(0);
			break;
		case "de_DE":
			languageChoisBox.getSelectionModel().select(1);
			break;
		default:
			languageChoisBox.getSelectionModel().select(0);
			break;
		}
		
		// initialize the sources table
		sourceColumn.setCellValueFactory(cellData -> cellData.getValue().pathProperty());
		modeColumn.setCellValueFactory(cellData -> cellData.getValue().modeProperty());
		sourcesTable.setItems(sourcesList);
		
		initActions();
		loadInitSources();
    }
    
    private void initActions() {
    	languageChoisBox.getSelectionModel().selectedIndexProperty().addListener((e, oldValue, newValue) -> {
			String local = languageChoisBox.getItems().get((int) newValue).toString();
			local = local.substring(local.length() - 6, local.length() - 1); // reading only en_US from English (en_US)

			XMLController.setUsrLocal(local);
			xmlController.saveSettings();
			MainWindowController.getInstance().setLocalUI();
		});
    	
    	branchChoisBox.getSelectionModel().selectedIndexProperty().addListener((e, oldValue, newValue) -> {
			if (branchChoisBox.getItems().get((int) newValue).toString() == "beta") {
				XMLController.setUseBeta(true);
			} else {
				XMLController.setUseBeta(false);
			}
			xmlController.saveSettings();
		});
    	
    	fontsizeSlider.valueProperty().addListener(e -> {
			XMLController.setFontSize(fontsizeSlider.getValue());
			xmlController.saveSettings();
			
			// TODO add functionality for postermode
		});
    }
    
    @FXML
    private void addDirectoryBtnAction(ActionEvent event) {
    	DirectoryChooser directoryChooser = new DirectoryChooser();
		directoryChooser.setTitle(XMLController.getLocalBundle().getString("addDirectory"));
		File selectedFolder = directoryChooser.showDialog(settingsScrollPane.getScene().getWindow());
		if (selectedFolder != null && selectedFolder.exists()) {
			addSource(selectedFolder.getPath(), "local");
		} else {
			LOGGER.error("The selected folder dosen't exist!");
		}
    }

    @FXML
    private void addStreamSourceBtnAction(ActionEvent event) {
    	FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle(XMLController.getLocalBundle().getString("addStreamSource"));
		File selectedFile = fileChooser.showOpenDialog(settingsScrollPane.getScene().getWindow());
		if (selectedFile != null && selectedFile.exists()) {
			addSource(selectedFile.getPath(), "stream");
		} else {
			LOGGER.error("The selected file dosen't exist!");
		}
    }

    @FXML
    private void autoUpdateToggleBtnAction(ActionEvent event) {
    	XMLController.setAutoUpdate(!XMLController.isAutoUpdate());
		xmlController.saveSettings();
    }

    @FXML
    private void autoplayToggleBtnAction(ActionEvent event) {
    	XMLController.setAutoplay(!XMLController.isAutoplay());
		xmlController.saveSettings();
    }
    
    @FXML
    private void fullscreenToggleBtnAction(ActionEvent event) {
    	XMLController.setFullscreen(!XMLController.isFullscreen());
		xmlController.saveSettings();
    }

    @FXML
    private void colorPickerAction(ActionEvent event) {
    	XMLController.setColor(colorPicker.getValue().toString().substring(2, 10));
		xmlController.saveSettings();
		MainWindowController.getInstance().applyColor();
    }

    @FXML
    private void updateBtnAction(ActionEvent event) {
		if (UpdateController.isUpdateAvailable()) {
			updateBtn.setText(XMLController.getLocalBundle().getString("updateBtnUpdateAvailable"));
			UpdateController.update();
		} else {
			updateBtn.setText(XMLController.getLocalBundle().getString("updateBtnNoUpdateAvailable"));
		}
    }
    
    /** TODO can this be done async?
	 * add a source to the sources file and load all new streams
	 * @param path to the source
	 * @param mode of the source (local or streaming)
	 */
	void addSource(String path, String mode) {
		JsonArray newsources = null;

		try {settingsScrollPane.setVbarPolicy(ScrollBarPolicy.ALWAYS);
			// read old array
			File oldSources = new File(XMLController.getDirHomeFlix() + "/sources.json");
			newsources = oldSources.exists() ? Json.parse(new FileReader(XMLController.getDirHomeFlix() + "/sources.json")).asArray() : Json.array();

			// add new source
			JsonObject source = Json.object().add("path", path).add("mode", mode);
			newsources.add(source);
			Writer writer = new FileWriter(XMLController.getDirHomeFlix() + "/sources.json");
			newsources.writeTo(writer);
			writer.close();
		} catch (IOException e) {
			LOGGER.error(e);
		}

		// update the sourcesTable
		sourcesList.add(new SourceDataType(path, mode));
		
		DBController.getInstance().refreshDataBase(); // refreshes the database after a source path was added
		MainWindowController.getInstance().checkAllPosters(); // check if there is anything new to cache
		MainWindowController.getInstance().addAllPosters();
	}
    
    // add a all elements of sourcesList to the sources table on the settings pane
 	void loadInitSources() {
 		if(new File(XMLController.getDirHomeFlix() + "/sources.json").exists()) {
 			try {
 				// create a JsonArray, containing all sources, add each source to the mwc, get all films from it
 				JsonArray sources = Json.parse(new FileReader(XMLController.getDirHomeFlix() + "/sources.json")).asArray();
 				for (JsonValue source : sources) {
 					String path = source.asObject().getString("path", "");
 					String mode = source.asObject().getString("mode", "");
 					sourcesList.add(new SourceDataType(path, mode));
 				}
 			} catch (Exception e) {
 				e.printStackTrace();
 			}
 		}
 	}
    
    public void updateColor(String btnStyle) {
    	updateBtn.setStyle(btnStyle);
		addDirectoryBtn.setStyle(btnStyle);
		addStreamSourceBtn.setStyle(btnStyle);
		
		autoUpdateToggleBtn.setToggleColor(Color.valueOf(XMLController.getColor()));
		autoUpdateToggleBtn.setToggleLineColor(Color.valueOf(XMLController.getColor()));
		autoplayToggleBtn.setToggleColor(Color.valueOf(XMLController.getColor()));
		autoplayToggleBtn.setToggleLineColor(Color.valueOf(XMLController.getColor()));
		fullscreenToggleBtn.setToggleColor(Color.valueOf(XMLController.getColor()));
		fullscreenToggleBtn.setToggleLineColor(Color.valueOf(XMLController.getColor()));
    }
    
    public void updateGUILocal() {
    	homeflixSettingsLbl.setText(XMLController.getLocalBundle().getString("homeflixSettingsLbl"));
    	mainColorLbl.setText(XMLController.getLocalBundle().getString("mainColorLbl"));
		fontsizeLbl.setText(XMLController.getLocalBundle().getString("fontsizeLbl"));
		languageLbl.setText(XMLController.getLocalBundle().getString("languageLbl"));
		branchLbl.setText(XMLController.getLocalBundle().getString("branchLbl"));
		sourcesLbl.setText(XMLController.getLocalBundle().getString("sourcesLbl"));
		
		updateBtn.setText(XMLController.getLocalBundle().getString("checkUpdates"));
		addDirectoryBtn.setText(XMLController.getLocalBundle().getString("addDirectory"));
		addStreamSourceBtn.setText(XMLController.getLocalBundle().getString("addStreamSource"));
		
		autoUpdateToggleBtn.setText(XMLController.getLocalBundle().getString("autoUpdate"));
		autoplayToggleBtn.setText(XMLController.getLocalBundle().getString("autoplay"));
		fullscreenToggleBtn.setText(XMLController.getLocalBundle().getString("fullscreen"));
    }
    
    public void setVisible(boolean visible) {
    	settingsScrollPane.setVisible(visible);
    }
    
    public boolean isVisible() {
    	return settingsScrollPane.isVisible();
    }
    
    public JFXButton getUpdateBtn() {
    	return updateBtn;
    }

}
