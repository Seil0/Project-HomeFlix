/**
 * Project-HomeFlix
 *
 * Copyright 2016-2022  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

package org.mosad.homeflix.application;

import java.io.File;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mosad.homeflix.controller.DBController;
import org.mosad.homeflix.controller.XMLController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {

	private Scene scene;
	private AnchorPane pane;
	private MainWindowController mainWindowController;
	private static XMLController xmlController;
	private static Logger LOGGER;
	public static final String version = "0.8.91";
	public static final String buildNumber = "177";
	public static final String versionName = "nimbly xlr8";
	
	// TODO rename streamURL to mediaURL

	@Override
	public void start(Stage primaryStage) {
		
		//initialize the mainWindowController and the primaryStage
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/fxml/MainWindow.fxml"));
			pane = (AnchorPane) loader.load();
			primaryStage.setMinHeight(600.00 + 34); // 34 -> window decoration
			primaryStage.setMinWidth(1130.00);
			//primaryStage.setResizable(false);
			primaryStage.setTitle("Project HomeFlix");
			primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("/icons/Homeflix_Icon_64x64.png"))); //adds application icon	
			primaryStage.setOnCloseRequest(event -> {
				DBController.getInstance().closeDBConnection();
				System.exit(0);
			});

			// generate window
			scene = new Scene(pane); // create new scene, append pane to scene
			scene.getStylesheets().add(getClass().getResource("/css/MainWindow.css").toExternalForm());
			primaryStage.setScene(scene); // append scene to stage
			primaryStage.show(); // show stage
			
			mainWindowController = loader.getController();	//Link of FXMLController and controller class
			mainWindowController.init();
		} catch (IOException e) {
			LOGGER.error("Error while loading in Main", e);
		}
	}

	/**
	 * set the log file location and initialize the logger launch the GUI
	 * @param args arguments given at the start
	 */
	public static void main(String[] args) {
		// Logger initialization
		String logPath = "";
		
		if (System.getProperty("os.name").contains("Windows")) {
			logPath = System.getProperty("user.home") + "/Documents/HomeFlix/app.log";
		} else {
			logPath = System.getProperty("user.home") + "/HomeFlix/app.log";
		}
		
		System.setProperty("logFilename", logPath);
		File logFile = new File(logPath);
		logFile.delete();
		LOGGER = LogManager.getLogger(Main.class.getName());
		
		LOGGER.info("OS: " + XMLController.getOsName() + " " + XMLController.getOsVers() + " " + XMLController.getOsVers());
		LOGGER.info("Java: " + XMLController.getJavaVend() + " " + XMLController.getJavaVers());
		LOGGER.info("User: " + XMLController.getUserName() + " " + XMLController.getUserHome());
		
		xmlController = new XMLController(); // Initialize the XMLController
		
		// TODO run Migrations
		
		if (!XMLController.getConfigFile().exists()) {
			xmlController.saveSettings(); // save the settings file with default values if it doesn't exist
		}
		
		xmlController.loadSettings();

		if (!XMLController.getPosterCache().exists()) {
			XMLController.getPosterCache().mkdir();
		}
		
		launch();
	}

}
