/**
 * Project-HomeFlix
 *
 * Copyright 2016-2022  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

package org.mosad.homeflix.datatypes;

import com.jfoenix.controls.JFXButton;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class PosterModeElement extends VBox{
	
	private String streamURL;
	private String title;
	private Label label = new Label();
	private JFXButton button = new JFXButton();
	private ImageView imageView = new ImageView();
	
	public PosterModeElement() {
		super.getChildren().addAll(label, button);
		
		label.setMaxWidth(200);
		label.setPadding(new Insets(0,0,0,8));
		label.setFont(Font.font("System", FontWeight.BOLD, 14));
		
		imageView.setFitHeight(300);
		imageView.setFitWidth(200);
		
		button.setStyle("-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.8), 10, 0, 0, 3); ");
		button.setGraphic(imageView);
	}
	
	public PosterModeElement(String streamURL, String title, Image poster) {
		this();
		
		this.streamURL = streamURL;
		this.title = title;
		
		label.setText(title);
		imageView.setImage(poster);
	}
	
	public String getStreamURL() {
		return streamURL;
	}
	
	public String getTitle() {
		return title;
	}

	public Label getLabel() {
		return label;
	}

	public JFXButton getButton() {
		return button;
	}

	public ImageView getImageView() {
		return imageView;
	}

	public void setStreamURL(String streamURL) {
		this.streamURL = streamURL;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public void setButton(JFXButton button) {
		this.button = button;
	}

	public void setImageView(ImageView imageView) {
		this.imageView = imageView;
	}

}
