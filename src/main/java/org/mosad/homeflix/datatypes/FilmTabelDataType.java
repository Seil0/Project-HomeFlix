/**
 * Project-HomeFlix
 * 
 * Copyright 2016-2022  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */
package org.mosad.homeflix.datatypes;

public class FilmTabelDataType {
	private String streamUrl;
	private String title;
	private String season;
	private String episode;
	
	/**
	 * tableData is the data-type of tree-table-view
	 * @param streamUrl the concrete path to the file or the URL
	 * @param title title of the film
	 * @param season season if it's a series
	 * @param episode episode if it's a series
	 */
	public FilmTabelDataType(String streamUrl, String title, String season, String episode) {
		this.streamUrl = streamUrl;
		this.title = title;
		this.season = season;
		this.episode = episode;
	}
	
	public String getStreamUrl() {
		return streamUrl;
	}
	
	public String getTitle() {
		return title;
	}

	public String getSeason() {
		return season;
	}
	
	public String getEpisode() {
		return episode;
	}
	
	public void setStreamUrl(String streamUrl) {
		this.streamUrl = streamUrl;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setSeason(String season) {
		this.season = season;
	}
	
	public void setEpisode(String episode) {
		this.episode = episode;
	}

}
