package org.mosad.homeflix.datatypes;

import org.mosad.homeflix.player.Player;

import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class SeriesDVEpisode extends AnchorPane {
	
	private String streamURL;
	private Label label = new Label();
	private ImageView imageView = new ImageView();
	
	public SeriesDVEpisode() {
		super.getChildren().addAll(imageView, label);
		super.prefWidth(200);
		super.prefHeight(112);

		
		imageView.setPreserveRatio(true);
		imageView.setFitHeight(112);
		
		label.setStyle("-fx-text-fill: #ffffff; -fx-font-size: 14pt ; -fx-font-weight: bold;");
		super.setTopAnchor(label, 3.0); 
		super.setLeftAnchor(label, 7.0);
	}
	
	public SeriesDVEpisode(String streamURL, String episode, Image poster) {
		this();
		
		this.streamURL = streamURL;
		
		label.setText(episode);
		imageView.setImage(poster);
		imageView.addEventHandler(MouseEvent.MOUSE_PRESSED, (e) -> {
			// Always play with the integrated Player TODO
			new Player(streamURL);
		});
	}

	public String getStreamURL() {
		return streamURL;
	}

	public Label getLabel() {
		return label;
	}

	public ImageView getImageView() {
		return imageView;
	}

	public void setStreamURL(String streamURL) {
		this.streamURL = streamURL;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public void setImageView(ImageView imageView) {
		this.imageView = imageView;
	}

}
