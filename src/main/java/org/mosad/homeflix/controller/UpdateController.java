/**
 * Project-HomeFlix
 * 
 * Copyright 2018-2019  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package org.mosad.homeflix.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.swing.ProgressMonitor;
import javax.swing.ProgressMonitorInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mosad.homeflix.application.Main;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class UpdateController {

	private static int updateBuildNumber; // tag_name from gitea
	@SuppressWarnings("unused")
	private static String updateName;
	@SuppressWarnings("unused")
	private static String updateChanges;
	private static String browserDownloadUrl; // update download link	
	private static final String giteaApiRelease = "https://git.mosad.xyz/api/v1/repos/Seil0/Project-HomeFlix/releases";
	
	private static final Logger LOGGER = LogManager.getLogger(UpdateController.class.getName());

	/**
	 * updater for Project HomeFlix based on cemu_UIs, checks for Updates and download it
	 */
	
	public static void update() {
		if (browserDownloadUrl != null) {
			LOGGER.info("download link: " + browserDownloadUrl);
			
			Runnable run = () -> {
				try {
					// open new HTTP connection, ProgressMonitorInputStream for downloading the data
					// FIXME the download progress dialog is not showing!
					HttpURLConnection connection = (HttpURLConnection) new URL(browserDownloadUrl).openConnection();
					ProgressMonitorInputStream pmis = new ProgressMonitorInputStream(null, "Downloading...", connection.getInputStream());
					ProgressMonitor pm = pmis.getProgressMonitor();
					pm.setMillisToDecideToPopup(0);
					pm.setMillisToPopup(0);
					pm.setMinimum(0);// set beginning of the progress bar to 0
					pm.setMaximum(connection.getContentLength());// set the end to the file length
					FileUtils.copyInputStreamToFile(pmis, new File("ProjectHomeFlix_update.jar")); // download update
					LOGGER.info("update download successful, restarting ...");
					org.apache.commons.io.FileUtils.copyFile(new File("ProjectHomeFlix_update.jar"), new File("ProjectHomeFlix.jar"));
					org.apache.commons.io.FileUtils.deleteQuietly(new File("ProjectHomeFlix_update.jar")); // delete update
					new ProcessBuilder("java", "-jar", "ProjectHomeFlix.jar").start(); // start the new application
					System.exit(0); // close the current application
				} catch (IOException e) {
					LOGGER.info("could not download update files", e);
				}
			};
			
			Thread t = new Thread(run);
			t.start();
			
		}
	}
	
	
	public static boolean isUpdateAvailable() {
		LOGGER.info("beta:" + XMLController.isUseBeta() + "; checking for updates ...");
		String apiOutput = "";
		
		try {
			URL giteaApiUrl = new URL(giteaApiRelease);
			
			BufferedReader ina = new BufferedReader(new InputStreamReader(giteaApiUrl.openStream()));
			apiOutput = ina.readLine();
			ina.close();
		} catch (Exception e) {
				LOGGER.error("could not check update version", e);
		}
		
		JsonArray objectArray = Json.parse("{\"items\": " + apiOutput + "}").asObject().get("items").asArray();
		JsonValue object = objectArray.get(0).asObject(); // set to the latest release as default
		JsonObject objectAsset = object.asObject().get("assets").asArray().get(0).asObject();
		
		// TODO rework stream()
		for(JsonValue objectIt : objectArray) {
			if(objectIt.asObject().getBoolean("prerelease", false) == XMLController.isUseBeta()) {
				// we found the needed release either beta or not
				object = objectIt;
				objectAsset = objectIt.asObject().get("assets").asArray().get(0).asObject();
				break;
			}
		}
		
		updateBuildNumber = Integer.parseInt(object.asObject().getString("tag_name", ""));
		updateName = object.asObject().getString("name", "");
		updateChanges = object.asObject().getString("body", "");
		browserDownloadUrl = objectAsset.getString("browser_download_url", "");
		
		LOGGER.info("Build: " + Main.buildNumber + ", Update: " + updateBuildNumber);
		
		return Integer.parseInt(Main.buildNumber) < updateBuildNumber;
		
	}
}
