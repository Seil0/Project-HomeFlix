/**
 * Project-HomeFlix
 *
 * Copyright 2016-2022  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

package org.mosad.homeflix.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;

public class XMLController {
	
	private static final String userHome = System.getProperty("user.home");
	private static final String userName = System.getProperty("user.name");
	private static final String osName = System.getProperty("os.name");
	private static final String osArch = System.getProperty("os.arch");
	private static final String osVers = System.getProperty("os.version");
	private static final String javaVers = System.getProperty("java.version");
	private static final String javaVend = System.getProperty("java.vendor");
	private static final String sysLocal = System.getProperty("user.language") + "_" + System.getProperty("user.country");
	private static String dirHomeFlixPath;
	private static File dirHomeFlix;
	private static File configFile;
	private static File posterCache;
	
	// user settings
	private static String color = "ee3523";
	private static String usrLocal = sysLocal;
	private static boolean autoUpdate = false;
	private static boolean useBeta = false;
	private static boolean autoplay = false;
	private static boolean fullscreen = true;
	private static double fontSize = 17;
	private static ResourceBundle localBundle = ResourceBundle.getBundle("locals.HomeFlix-Local", Locale.US);
	
	// api settings
	private static String omdbAPIKey;
	
	private static final Logger LOGGER = LogManager.getLogger(XMLController.class.getName());
	private Properties props = new Properties();
	
	/**
	 * prepare the start up, this is the first thing call by main
	 */
	public XMLController() {
		
		if (osName.contains("Windows")) {
			dirHomeFlixPath = userHome + "/Documents/HomeFlix";
		} else {
			dirHomeFlixPath = userHome + "/HomeFlix";
		}
		
		// set the concrete files
		dirHomeFlix = new File(dirHomeFlixPath);
		configFile = new File(dirHomeFlix + "/config.xml");
		posterCache = new File(dirHomeFlix + "/posterCache");
		
	}
	
	/**
	 * save the configuration to the config.xml file
	 */
	public void saveSettings() {
		LOGGER.info("saving settings ...");
		try {
			props.setProperty("color", color);
			props.setProperty("autoUpdate", String.valueOf(autoUpdate));
			props.setProperty("useBeta", String.valueOf(useBeta));
			props.setProperty("autoplay", String.valueOf(autoplay));
			props.setProperty("size", Double.toString(fontSize));
			props.setProperty("local", usrLocal);

			OutputStream outputStream = new FileOutputStream(getConfigFile()); // new output-stream
			props.storeToXML(outputStream, "Project HomeFlix settings"); // write new .xml
			outputStream.close();
		} catch (IOException e) {
			LOGGER.error("An error occurred while saving the settings!", e);
		}
	}
	
	/**
	 * load the configuration from the config.xml file
	 * and try to load the API keys from apiKeys.json
	 */
	public void loadSettings() {
		LOGGER.info("loading settings ...");
		
		try {
			InputStream inputStream = new FileInputStream(getConfigFile());
			props.loadFromXML(inputStream); // new input-stream from .xml

			try {
				setColor(props.getProperty("color"));
			} catch (Exception e) {
				LOGGER.error("cloud not load color", e);
				setColor("00a8cc");
			}

			try {
				setFontSize(Double.parseDouble(props.getProperty("size")));
			} catch (Exception e) {
				LOGGER.error("cloud not load fontsize", e);
				setFontSize(17.0);
			}

			try {
				setAutoUpdate(Boolean.parseBoolean(props.getProperty("autoUpdate")));
			} catch (Exception e) {
				LOGGER.error("cloud not load autoUpdate", e);
				setAutoUpdate(false);
			}
			
			try {
				setUseBeta(Boolean.parseBoolean(props.getProperty("useBeta")));
			} catch (Exception e) {
				LOGGER.error("cloud not load autoUpdate", e);
				setUseBeta(false);
			}
			
			try {
				setAutoplay(Boolean.parseBoolean(props.getProperty("autoplay")));
			} catch (Exception e) {
				LOGGER.error("cloud not load autoplay", e);
				setAutoplay(false);
			}

			try {
				setUsrLocal(props.getProperty("local"));
			} catch (Exception e) {
				LOGGER.error("cloud not load local", e);
				setUsrLocal(System.getProperty("user.language") + "_" + System.getProperty("user.country"));
			}

			inputStream.close();
		} catch (IOException e) {
			LOGGER.error("An error occurred while loading the settings!", e);
		}
		
		// try loading the omdbAPI key
		try {
			InputStream in = getClass().getClassLoader().getResourceAsStream("apiKeys.json");
			if (in != null) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(in));
				JsonObject apiKeys = Json.parse(reader).asObject();
				setOmdbAPIKey(apiKeys.getString("omdbAPIKey", ""));
				reader.close();
				in.close();
			} else {
				LOGGER.warn("Cloud not load apiKeys.json. No such file");
			}
		} catch (Exception e) {
			LOGGER.error("Cloud not load the omdbAPI key. Please contact the developer!", e);
		}
	}
	
	// getters for application variables
	
	public static String getUserHome() {
		return userHome;
	}

	public static String getUserName() {
		return userName;
	}

	public static String getOsName() {
		return osName;
	}

	public static String getOsArch() {
		return osArch;
	}

	public static String getOsVers() {
		return osVers;
	}

	public static String getJavaVers() {
		return javaVers;
	}

	public static String getJavaVend() {
		return javaVend;
	}

	public static String getSysLocal() {
		return sysLocal;
	}
	
	public static String getDirHomeFlixPath() {
		return dirHomeFlixPath;
	}

	
	public static File getDirHomeFlix() {
		return dirHomeFlix;
	}

	public static File getConfigFile() {
		return configFile;
	}

	public static File getPosterCache() {
		return posterCache;
	}
	
	// getters for user settings

	public static String getColor() {
		return color;
	}

	public static void setColor(String color) {
		XMLController.color = color;
	}

	public static String getUsrLocal() {
		return usrLocal;
	}

	public static void setUsrLocal(String usrLocal) {
		XMLController.usrLocal = usrLocal;
	}

	public static boolean isAutoUpdate() {
		return autoUpdate;
	}

	public static void setAutoUpdate(boolean autoUpdate) {
		XMLController.autoUpdate = autoUpdate;
	}

	public static boolean isUseBeta() {
		return useBeta;
	}

	public static void setUseBeta(boolean useBeta) {
		XMLController.useBeta = useBeta;
	}

	public static boolean isAutoplay() {
		return autoplay;
	}

	public static void setAutoplay(boolean autoplay) {
		XMLController.autoplay = autoplay;
	}
	
	public static boolean isFullscreen() {
		return fullscreen;
	}
	
	public static void setFullscreen(boolean fullscreen) {
		XMLController.fullscreen = fullscreen;
	}

	public static double getFontSize() {
		return fontSize;
	}

	public static void setFontSize(double fontSize) {
		XMLController.fontSize = fontSize;
	}
	
	public static ResourceBundle getLocalBundle() {
		return localBundle;
	}

	public static void setLocalBundle(ResourceBundle localBundle) {
		XMLController.localBundle = localBundle;
	}


	// getters for APIs
	
	public static String getOmdbAPIKey() {
		return omdbAPIKey;
	}

	private static void setOmdbAPIKey(String omdbAPIKey) {
		XMLController.omdbAPIKey = omdbAPIKey;
	}
}
