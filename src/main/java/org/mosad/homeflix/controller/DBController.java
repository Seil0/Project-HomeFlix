/**
 * Project-HomeFlix
 * 
 * Copyright 2016-2022  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 */

package org.mosad.homeflix.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mosad.homeflix.datatypes.DatabaseDataType;
import org.mosad.homeflix.datatypes.FilmTabelDataType;
import org.mosad.homeflix.datatypes.OMDbAPIResponseDataType;
import org.mosad.homeflix.datatypes.PosterModeElement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

public class DBController {
	
	private static DBController instance = null;
	private String DB_PATH;
	private List<DatabaseDataType> databaseStreams = new ArrayList<DatabaseDataType>(); // contains all films stored in the database
	private List<DatabaseDataType> sourceStreams = new ArrayList<DatabaseDataType>(); // contains all films from the sources
	private Connection connection = null;
	private static final Logger LOGGER = LogManager.getLogger(DBController.class.getName());
	
	/**
	 * constructor for DBController
	 */
	public DBController() {	
		// Auto-generated constructor stub
	}
	
	public static DBController getInstance() {
		if (instance == null) {
			instance = new DBController();
		}
		
		return instance;
	}
	
	/**
	 * initialize the {@link DBController} with the database connection check if
	 * there is a need to create a new database refresh the database
	 */
	public void init() {
		initDatabaseConnection();
		createDatabase();
	}

	/**
	 * create a new connection to the HomeFlix.db database
	 * AutoCommit is set to false to prevent some issues, so manual commit is active!
	 */
	private void initDatabaseConnection() {
		DB_PATH = XMLController.getDirHomeFlix() + "/Homeflix.db";
		try {
			// create a database connection
			connection = DriverManager.getConnection("jdbc:sqlite:" + DB_PATH);
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			// if the error message is "out of memory", it probably means no database file is found
			LOGGER.error("error while loading the HomeFlix database", e);
		}
		LOGGER.info("HomeFlix database loaded successfull");
	}
	
	/**
	 * if tables don't exist create them
	 * films table: streamUrl is primary key
	 * cache table: streamUrl is primary key
	 */
	private void createDatabase() {
		try (Statement stmt = connection.createStatement();) {
			stmt.executeUpdate("create table if not exists films (streamUrl, title, season, episode, favorite, cached, currentTime)");
			stmt.executeUpdate("create table if not exists cache ("
					+ "streamUrl, Title, Year, Rated, Released, Season, Episode ,Runtime, Genre, Director, Writer,"
					+ " Actors, Plot, Language, Country, Awards, Poster, Metascore, imdbRating, imdbVotes,"
					+ " imdbID, Type, dvd, BoxOffice, Website, Response)");
			connection.commit();
		} catch (SQLException e) {
			LOGGER.error(e);
		}
	}
	
	/**
	 * get all database entries
	 */
	private void loadDatabase() {
		// get all entries from the table
		try (Statement stmt = connection.createStatement();) {
			
			try (ResultSet rs = stmt.executeQuery("SELECT * FROM films");) {
				while (rs.next()) {
					databaseStreams.add(new DatabaseDataType(rs.getString("streamUrl"),
							rs.getString("title"), rs.getString("season"), rs.getString("episode"),
							rs.getInt("favorite"), rs.getDouble("currentTime")));
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error("Ups! an error occured!", e);
		}
	}
	
	/**
	 * load all source streams
	 */
	private void loadSources() {
		SourcesController sourcesController = new SourcesController();
		sourceStreams = sourcesController.loadSources();
	}
	
	/**
	 * try to close the databse connection
	 */
	public void closeDBConnection( ) {
		try {
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			LOGGER.error("Error while trying to close the databse connection", e);
		}
	}
	
	/**
	 * load all streams from the database to a ObservableList, order entries by title
	 * @return a ObservableList that contains all streams from the database
	 */
	public ObservableList<FilmTabelDataType> getStreamsList() {
		ObservableList<FilmTabelDataType> filmsList = FXCollections.observableArrayList();
		LOGGER.info("loading data from database ...");
		
		//load local Data
		try (Statement stmt = connection.createStatement(); ) {
			
			try (ResultSet rs = stmt.executeQuery("SELECT * FROM films ORDER BY title");) {
				while (rs.next()) {			
					filmsList.add(new FilmTabelDataType(rs.getString("streamUrl"),
							rs.getString("title"), rs.getString("season"), rs.getString("episode")));
				}
			}
				
		} catch (SQLException e) {
			LOGGER.error("Ups! an error occured!", e);
		}
		
		return filmsList;
	}
	
	/**
	 * get one stream from the database with streamUrl = ?
	 * @param streamUrl of the stream
	 * @return a FilmTabelDataType Object of the stream with the given streamUrl
	 */
	public FilmTabelDataType getStream(String streamUrl) {
		FilmTabelDataType film = null;
		LOGGER.info("refresh data for " + streamUrl);
		try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM films WHERE streamUrl = ?");) {
			ps.setString(1, streamUrl);
			
			try (ResultSet rs = ps.executeQuery();) {
				while (rs.next()) {
					film = new FilmTabelDataType(rs.getString("streamUrl"),
							rs.getString("title"), rs.getString("season"), rs.getString("episode"));
				}
			}
			
		} catch (Exception e) {
			LOGGER.error("Ups! error while refreshing mwc!", e);
		}
		
		return film;
	}
	
	/**
	 * get entries which have no season and episode eg. root or movie of the database as PosterModeElement
	 * @return a ObservableList of PosterModeElements
	 */
	public ObservableList<PosterModeElement> getPosterElementsList() {
		ObservableList<PosterModeElement> posterElementsList = FXCollections.observableArrayList();
		LOGGER.info("loading data from database ...");
		
		//load local Data
		try (Statement stmt = connection.createStatement();) {
			
			try (ResultSet rs = stmt.executeQuery("SELECT * FROM films WHERE season = '' OR season = '0'  ORDER BY title");) {
				while (rs.next()) {
					String[] cacheData = readCacheLegacy(rs.getString("streamUrl")); // get from the cache table
					
					if(cacheData[20] != null && cacheData[20].length() > 0) {
						posterElementsList.add(new PosterModeElement(rs.getString("streamUrl"), cacheData[0], new Image(new File(cacheData[20]).toURI().toString())));
					} else {
						posterElementsList.add(new PosterModeElement(rs.getString("streamUrl"), cacheData[0], new Image("icons/Homeflix_Poster.png")));
					}
				}
			}

		} catch (SQLException e) {
			LOGGER.error("An error occured while getting all PosterElements!", e);
		}
		
		return posterElementsList;
	}
	
	/**
	 * refresh database to contain all (new added) entries
	 */
	public void refreshDataBase() {
		LOGGER.info("<========== starting refreshing database ==========>");
		
		// clean all ArraLists
		databaseStreams.clear();
		sourceStreams.clear();
		
		loadSources(); // reload all sources
		loadDatabase(); // reload all films saved in the DB
		
		LOGGER.info("There are {} entries in the Database", databaseStreams.size());
		
		try {
			checkAddEntry();
			checkRemoveEntry();
		} catch (Exception e) {
			LOGGER.error("Error while refreshing the database", e);
		}
		
		LOGGER.info("<========== finished refreshing database ==========>");
	}

	/**
	 * check if there are any entries that have been removed from the film-directory
	 * @throws SQLException 
	 */
	private void checkRemoveEntry()  {
		LOGGER.info("checking for entrys to remove to DB ...");
		
		try (PreparedStatement ps = connection.prepareStatement("DELETE FROM films WHERE streamUrl = ?");) {
			for (DatabaseDataType dbStreamEntry : databaseStreams) {
				// if the directory doen't contain the entry form the database, remove it
				
				// if sourceStreams has a item where StreamUrl equals dbStreamEntry.getStreamUrl() return it, else null
				DatabaseDataType result = sourceStreams.stream()
						.filter(x -> dbStreamEntry.getStreamUrl().equals(x.getStreamUrl()))
						.findAny()
						.orElse(null);
				
				// if the result is null, the file is missing, remove it from the database
				if (result == null) {
					ps.setString(1, dbStreamEntry.getStreamUrl());
					ps.addBatch();
					LOGGER.info("removed \"" + dbStreamEntry.getTitle() + "\" from database");
				}
			}
			
			ps.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			LOGGER.error("An error occured, while cheking for media to remove", e);
		}
	}
	
	/**
	 * check for new media in the source directories
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void checkAddEntry() throws FileNotFoundException, IOException {
		LOGGER.info("checking for entrys to add to DB ...");
		
		try (PreparedStatement ps = connection.prepareStatement("insert into films values (?, ?, ?, ?, ?, ?, ?)");) {
			for (DatabaseDataType sourceStreamEntry : sourceStreams) {
				
				// if databaseStream has a item where StreamUrl equals sourceStreamEntry.getStreamUrl() return it, else null
				DatabaseDataType result = databaseStreams.stream()
						.filter(x -> sourceStreamEntry.getStreamUrl().equals(x.getStreamUrl()))
						.findAny()
						.orElse(null);
				
				// if the result is null, the entry is missing, add it to the database
				if (result == null) {
					ps.setString(1, sourceStreamEntry.getStreamUrl());
					ps.setString(2, sourceStreamEntry.getTitle());
					ps.setString(3, sourceStreamEntry.getSeason());
					ps.setString(4, sourceStreamEntry.getEpisode());
					ps.setInt(5, sourceStreamEntry.getFavorite());
					ps.setDate(6, new Date(0));
					ps.setDouble(7, sourceStreamEntry.getCurrentTime());
					ps.addBatch(); // adds the entry
					LOGGER.info("Added \"" + sourceStreamEntry.getTitle() + "\" to database");
					databaseStreams.add(sourceStreamEntry);
				}
			}
			
			ps.executeBatch();
			connection.commit();
		} catch (SQLException e) {
			LOGGER.error("An error occured, while cheking for new media", e);
		}
	}
	
	/**
	 * DEBUG
	 * prints all entries from the database to the console
	 */
	public void printAllDBEntriesDEBUG() {
		System.out.println("Outputting all entries ... \n");
		try (Statement stmt = connection.createStatement();) {
			
			try (ResultSet rs = stmt.executeQuery("SELECT * FROM films");) {
				while (rs.next()) {
					System.out.println(rs.getString("streamUrl"));
					System.out.println(rs.getString("title"));
					System.out.println(rs.getString("season"));
					System.out.println(rs.getString("episode"));
					System.out.println(rs.getString("rating"));
					System.out.println(rs.getString("cached"));
					System.out.println(rs.getString("currentTime") + "\n");
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error("An error occured, while printing all entries", e);
		}
	}
	
	/**
	 * return the favorite status of a media entry
	 * @param mediaURL URL of the media
	 * @return 0 if it's not a favorite, else 1
	 */
	public int getFavoriteState(String mediaURL) {
		int favoriteState = 0;
		
		try (PreparedStatement ps = connection.prepareStatement("SELECT favorite FROM films WHERE streamUrl = ?");) {
			ps.setString(1, mediaURL);
			
			try (ResultSet rs = ps.executeQuery();) {
				while (rs.next()) {
					favoriteState = rs.getInt("favorite");
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error("Ups! an error occured!", e);
		}

		return favoriteState;
	}
	
	public void toggleFavoriteState(String streamURL) {
		int fav = getFavoriteState(streamURL) == 0 ? 1 : 0;
		
		try (PreparedStatement ps = connection.prepareStatement("UPDATE films SET favorite = ? WHERE streamUrl = ?");) {
			ps.setInt(1, fav);
			ps.setString(2, streamURL);
			ps.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			LOGGER.error("Ups! an error occured!", e);
		}
	}
	
	/**
	 * update the database entry for the given film, favorite = 0
	 * @param streamUrl URL of the film
	 */
	public void dislike(String streamUrl) {
		LOGGER.info("dislike " + streamUrl);
		
		try (PreparedStatement ps = connection.prepareStatement("UPDATE films SET favorite = 0 WHERE streamUrl = ?");){
			ps.setString(1, streamUrl);
			ps.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			LOGGER.error("Ups! an error occured!", e);
		}
	}
	
	/**
	 * update the database entry for the given film, favorite = 1
	 * @param streamUrl URL of the film
	 */
	public void like(String streamUrl) {
		LOGGER.info("like " + streamUrl);
		
		try (PreparedStatement ps = connection.prepareStatement("UPDATE films SET favorite = 1 WHERE streamUrl = ?");) {
			ps.setString(1, streamUrl);
			ps.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			LOGGER.error("Ups! An error occured!", e);
		}
	}
	
	/**
	 * update the database entry for the given film, cached = 1
	 * @param streamUrl URL of the film
	 */
	public void setCached(String streamUrl) {
		try (PreparedStatement ps = connection.prepareStatement("UPDATE films SET cached = ? WHERE streamUrl = ?");) {
			ps.setDate(1, new Date(System.currentTimeMillis()));
			ps.setString(2, streamUrl);
			ps.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			LOGGER.error("Ups! An error occured, while setting cached = DATE for {}.", streamUrl, e);
		}
	}
	
	/**
	 * get the cached date for the film
	 * @param streamUrl URL of the film
	 */
	public LocalDate getCacheDate(String streamUrl) {
		LocalDate cacheDate = LocalDate.now().minusDays(31);
		
		try (PreparedStatement ps = connection.prepareStatement("SELECT cached FROM films WHERE streamUrl = ?");){
			ps.setString(1, streamUrl);
			
			try (ResultSet rs = ps.executeQuery();) {
				while (rs.next()) {
					cacheDate = rs.getDate("cached").toLocalDate();
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error("Ups! An error occured, while rerading the cached date for {}.", streamUrl, e);
		}
		
		return cacheDate;
	}
	
	/**
	 * add the received data to the cache table
	 * @param streamUrl 	URL of the film
	 * @param omdbResponse	the response data from omdbAPI
	 */
	public void addCache(String streamUrl, OMDbAPIResponseDataType omdbResponse) {
		LOGGER.info("adding cache for: " + streamUrl);
		
		try (PreparedStatement ps = connection.prepareStatement("insert into cache values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");){
			ps.setString(1,streamUrl);
			ps.setString(2,omdbResponse.getTitle());
			ps.setString(3,omdbResponse.getYear());
			ps.setString(4,omdbResponse.getRated());
			ps.setString(5,omdbResponse.getReleased());
			ps.setString(6,omdbResponse.getSeason());
			ps.setString(7,omdbResponse.getEpisode());
			ps.setString(8,omdbResponse.getRuntime());
			ps.setString(9,omdbResponse.getGenre());
			ps.setString(10,omdbResponse.getDirector());
			ps.setString(11,omdbResponse.getWriter());
			ps.setString(12,omdbResponse.getActors());
			ps.setString(13,omdbResponse.getPlot());
			ps.setString(14,omdbResponse.getLanguage());
			ps.setString(15,omdbResponse.getCountry());
			ps.setString(16,omdbResponse.getAwards());
			ps.setString(17,omdbResponse.getPoster());
			ps.setString(18,omdbResponse.getMetascore());
			ps.setString(19,omdbResponse.getImdbRating());
			ps.setString(20,omdbResponse.getImdbVotes());
			ps.setString(21,omdbResponse.getImdbID());		
			ps.setString(22,omdbResponse.getType());
			ps.setString(23,omdbResponse.getDvd());
			ps.setString(24,omdbResponse.getBoxOffice());
			ps.setString(25,omdbResponse.getWebsite());
			ps.setString(26,omdbResponse.getResponse());
			
			ps.addBatch();
			ps.executeBatch();			
			connection.commit();
		} catch (Exception e) {
			LOGGER.error("Ups! An error while adding cache data for {}.", streamUrl, e);
		}
	}
	
	/**
	 * checks if there is already a entry with the given streamUrl in the cache
	 * @param streamUrl URL of the element
	 * @return true if the element is already cached, else false
	 */
	public boolean searchCacheByURL(String streamUrl) {
		boolean retValue = false;
		try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM cache WHERE streamUrl = ?");) {
			ps.setString(1, streamUrl);
			
			try (ResultSet rs = ps.executeQuery();) {
				retValue = rs.next();
			}
			
		} catch (Exception e) {
			LOGGER.error("Ups! An error while searching for {} in the cache.", streamUrl, e);
		}
		
		return retValue;
	}
	
	/**
	 * read the cached data from the Database
	 * @param streamUrl URL of the film
	 * @return a String array (length = 21) with all cached data
	 */
	public String[] readCacheLegacy(String streamUrl) {
		String[] cacheData = new String[21];
		
		try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM cache WHERE streamUrl = ?");) {
			ps.setString(1, streamUrl);
			
			try (ResultSet rs = ps.executeQuery();) {
				while (rs.next()) {
					cacheData[0] = rs.getString("Title");
					cacheData[1] = rs.getString("Year");
					cacheData[2] = rs.getString("Rated");
					cacheData[3] = rs.getString("Released");
					cacheData[4] = rs.getString("Season");
					cacheData[5] = rs.getString("Episode");
					cacheData[6] = rs.getString("Runtime");
					cacheData[7] = rs.getString("Genre");
					cacheData[8] = rs.getString("Director");
					cacheData[9] = rs.getString("Writer");
					cacheData[10] = rs.getString("Actors");
					cacheData[11] = rs.getString("Plot");
					cacheData[12] = rs.getString("Language");
					cacheData[13] = rs.getString("Country");
					cacheData[14] = rs.getString("Awards");
					cacheData[15] = rs.getString("metascore");
					cacheData[16] = rs.getString("imdbRating");
					cacheData[17] = rs.getString("Type");
					cacheData[18] = rs.getString("BoxOffice");
					cacheData[19] = rs.getString("Website");
					cacheData[20] = rs.getString("Poster");
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error("Ups! error while getting the current time for {}.", streamUrl, e);
		}
		
		return cacheData;
	}
	
	public OMDbAPIResponseDataType readCache(String streamUrl) {
		OMDbAPIResponseDataType omdbResponse = new OMDbAPIResponseDataType();
		
		try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM cache WHERE streamUrl = ?");) {
			ps.setString(1, streamUrl);
			
			try (ResultSet rs = ps.executeQuery();) {
				while (rs.next()) {
					omdbResponse.setTitle(rs.getString("Title"));
					omdbResponse.setYear(rs.getString("Year"));
					omdbResponse.setRated(rs.getString("Rated"));
					omdbResponse.setReleased(rs.getString("Released"));
					omdbResponse.setSeason(rs.getString("Season"));
					omdbResponse.setEpisode(rs.getString("Episode"));
					omdbResponse.setRuntime(rs.getString("Runtime"));
					omdbResponse.setGenre(rs.getString("Genre"));
					omdbResponse.setDirector(rs.getString("Director"));
					omdbResponse.setWriter(rs.getString("Writer"));
					omdbResponse.setActors(rs.getString("Actors"));
					omdbResponse.setPlot(rs.getString("Plot"));
					omdbResponse.setLanguage(rs.getString("Language"));
					omdbResponse.setCountry(rs.getString("Country"));
					omdbResponse.setAwards(rs.getString("Awards"));
					omdbResponse.setMetascore(rs.getString("metascore"));
					omdbResponse.setImdbRating(rs.getString("imdbRating"));
					omdbResponse.setImdbVotes(rs.getString("imdbVotes"));
					omdbResponse.setImdbID(rs.getString("imdbID"));
					omdbResponse.setType(rs.getString("Type"));
					omdbResponse.setDvd(rs.getString("dvd"));
					omdbResponse.setBoxOffice(rs.getString("BoxOffice"));
					//omdbResponse.setProduction(object.getString("Production", ""));
					omdbResponse.setWebsite(rs.getString("Website"));
					omdbResponse.setResponse(rs.getString("Response"));
					omdbResponse.setPoster(rs.getString("Poster"));
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error("Ups! error while getting the current time for {}.", streamUrl, e);
		}
		
		return omdbResponse;
	}
	
	/**
	 * get all NOT cached entries
	 * @return a {@link ArrayList} of all NOT cached entries
	 */
	public ArrayList<FilmTabelDataType> getAllNotCachedEntries() {
		ArrayList<FilmTabelDataType> notCachedEntries = new ArrayList<>();
		
		try (Statement stmt = connection.createStatement();) {
			
			try (ResultSet rs = stmt.executeQuery("SELECT * FROM films WHERE cached = 0");) {
				while (rs.next()) {
					notCachedEntries.add(new FilmTabelDataType(rs.getString("streamUrl"),
							rs.getString("title"), rs.getString("season"), rs.getString("episode")));
				}
			}
			
		} catch (SQLException e) {
			LOGGER.error("An error occured, while getting all NOT cached entries", e);
		}
		
		LOGGER.info("There are {} entries not Chached!", notCachedEntries.size());
		return notCachedEntries;
	}
	
	/**
	 * return the currentTime in ms saved in the database
	 * @param streamUrl URL of the film
	 * @return {@link Double} currentTime in ms
	 */
	public double getCurrentTime(String streamUrl) {
		LOGGER.info("get currentTime for " + streamUrl);
		double currentTime = 0;
		
		try (PreparedStatement ps = connection.prepareStatement("SELECT * FROM films WHERE streamUrl = ?");) {
			ps.setString(1, streamUrl);
			
			try (ResultSet rs = ps.executeQuery();) {
				currentTime = rs.getDouble("currentTime");
			}
			
		} catch (Exception e) {
			LOGGER.error("Ups! error while getting the current time!", e);
		} 
		
		return currentTime;
	}
	
	/**
	 * save the currentTime to the database
	 * @param streamUrl		URL of the film
	 * @param currentTime	currentTime in ms of the film
	 */
	public void setCurrentTime(String streamUrl, double currentTime) {
		LOGGER.info("set currentTime = " + currentTime + " for " + streamUrl);
		
		try (PreparedStatement ps = connection.prepareStatement("UPDATE films SET currentTime = ? WHERE streamUrl = ?");) {
			ps.setDouble(1, currentTime);
			ps.setString(2, streamUrl);
			ps.executeUpdate();
			connection.commit();
		} catch (SQLException e) {
			LOGGER.error("Ups! error while updateing the current time!", e);
		}
	}
	
	/**
	 * return the cache data for all episodes of a series season
	 * @param title the title of the series
	 * @param season the season you want
	 * @return a ArrayList of cache data (String Array [21])
	 */
	public ArrayList<String[]> getEpisodes(String title, int season) {
		ArrayList<String[]> episodePosters =new ArrayList<>();
		
		// try to get a all episode of one season
		try (PreparedStatement ps = connection.prepareStatement("SELECT streamUrl, episode FROM films WHERE title = ? AND season = ?");) {
			ps.setString(1, title);
			ps.setString(2, Integer.toString(season));
			
			try (ResultSet rs = ps.executeQuery();) {
				// for each episode load cache data
				while (rs.next()) {
					String[] cacheData = readCacheLegacy(rs.getString("streamUrl"));
					String[] ret = {rs.getString("streamUrl"), rs.getString("episode"), cacheData[20]};
					episodePosters.add(ret);
				}
			}
		} catch(Exception e) {
			LOGGER.error("Ups! error while getting episodes of a season!", e);
		}
		
		return episodePosters;
	}
	
	/**
	 * get the next episode of a series
	 * @param title		title of the film
	 * @param episode	current episode
	 * @param season	season of the current episode
	 * @return {@link FilmTabelDataType} the next episode as object
	 */
	public FilmTabelDataType getNextEpisode(String title, int episode, int season) {
		FilmTabelDataType nextFilm = null;
		
		try {
			// try to get a new episode of the current season
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM films WHERE title = ? AND season = ? AND episode = ?");
			ps.setString(1, title);
			ps.setString(2, Integer.toString(season));
			ps.setString(3, Integer.toString(episode + 1));
			ResultSet rs = ps.executeQuery();
			
			/*  if that fails get the next season and try to get a episode there,
			 *  we need to test only for the next season, first episode,
			 *  any other entry would not exist in the database
			 */
			if(!rs.next()) {
				ps.setString(1, title);
				ps.setString(2, Integer.toString(season + 1));
				ps.setString(3, Integer.toString(1));
				rs = ps.executeQuery();
				if(!rs.next()) return nextFilm; // if we haven't found anything return an empty object
			}
			
			// at this point we have found the correct episode
			nextFilm = new FilmTabelDataType(rs.getString("streamUrl"), rs.getString("title"),
					rs.getString("season"), rs.getString("episode"));
			
			rs.close();
			ps.close();
		} catch (Exception e) {
			LOGGER.error("Ups! error while getting next episode!", e);
		}
		return nextFilm;
	}
	
	/** TODO rework, we should save the next episode in the root entry
	 * get the last watched episode
	 * @param streamURL URL of the stream
	 * @return the last watched episodes URL
	 */
	public String getLastWatchedEpisode(String streamURL) {
		LOGGER.info("last watched episode in: " + streamURL);
		String lastEp = null;
		double lastCurrentTime = 0;
		
		try {
			PreparedStatement ps = connection.prepareStatement("SELECT * FROM films WHERE streamUrl LIKE ?");
			ps.setString(1, streamURL + "/%");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				// get the first episode where currentTime != 0
				if (rs.getDouble("currentTime") > lastCurrentTime || lastEp == null) {
					lastEp = rs.getString("streamUrl");
				}
			}
			rs.close();
			ps.close();
		} catch (Exception e) {
			LOGGER.error("Ups! error while getting the last watched episode!", e);
		} 

		return lastEp;
	}
	
	/**
	 * check if a file is a video
	 * @param path the path to the file
	 * @return true if the file is a video, else false
	 */
	public static boolean isVideoFile(String path) {
	    String mimeType = URLConnection.guessContentTypeFromName(path);    
	    return mimeType != null && mimeType.startsWith("video");
	}
	
}
