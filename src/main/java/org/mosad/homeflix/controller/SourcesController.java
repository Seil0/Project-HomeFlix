/**
 * Project-HomeFlix
 *
 * Copyright 2016-2022  <@Seil0>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

package org.mosad.homeflix.controller;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mosad.homeflix.datatypes.DatabaseDataType;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class SourcesController {
	
	private List<DatabaseDataType> sourceStreams = new ArrayList<DatabaseDataType>();
	private static final Logger LOGGER = LogManager.getLogger(SourcesController.class.getName());
	
	public SourcesController() {
		// Auto-generated constructor stub
	}
	
	/**
	 * load all local and streaming sources and add them to a list
	 */
	public List<DatabaseDataType> loadSources() {
		
		try {
			// create a JsonArray, containing all sources, add each source to the mwc, get all films from it
			JsonArray sources = Json.parse(new FileReader(XMLController.getDirHomeFlix() + "/sources.json")).asArray();
			for (JsonValue source : sources) {
				String path = source.asObject().getString("path", "");
				String mode = source.asObject().getString("mode", "");
				
				if (mode.equals("local"))
					addLocalSource(path);
				if (mode.equals("stream"))
					addStreamSource(path);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		LOGGER.info("films in directory: " + sourceStreams.size());
		
		return sourceStreams;
	}
	/** 
	 * for each file in source path
	 * check whether it's valid file or a directory (series)
	 * if it's valid or a series add it to the sourceStreams
	 * @param	a local source path (a directory)
	 */
	private void addLocalSource(String path) {

		int oldSize = sourceStreams.size();
		for (File file : new File(path).listFiles()) {
			// if it's valid file add it to the sourceStreams
			if (isValidFile(file)) {
				sourceStreams.add(new DatabaseDataType(file.getPath(), cutOffEnd(file.getName().replace("_", " ")), "", "", 0, 0.0));
			} else if(isValidSeriesRoot(file)) {
				// get all directories (series), root and season must be directories
				sourceStreams.add(new DatabaseDataType(file.getPath(), file.getName().replace("_", " "), "0", "0", 0, 0.0)); // add the series root node
				int sn = 1;
				for (File season : file.listFiles()) {
					if (season.isDirectory()) {
						int ep = 1;
						for (File episode : season.listFiles()) {
							// check whether the episode is a valid file
							if (isValidFile(episode)) {
								sourceStreams.add(new DatabaseDataType(episode.getPath(), cutOffEnd(file.getName()),
										Integer.toString(sn), Integer.toString(ep), 0, 0.0));
								ep++;
							}
						}
						sn++;
					}
				}
			}
		}
		
		LOGGER.info("added " + (sourceStreams.size() - oldSize) + " local files from: " + path);
	}
	
	private void addStreamSource(String path) {
		// getting all entries from the streaming lists
		try {
			JsonObject object = Json.parse(new FileReader(path)).asObject();
			JsonArray items = object.get("entries").asArray();
			for (JsonValue item : items) {
				sourceStreams.add(new DatabaseDataType(item.asObject().getString("streamUrl", ""),
						item.asObject().getString("title", ""),
						item.asObject().getString("season", ""),
						item.asObject().getString("episode", ""), 0, 0.0));
			}
			LOGGER.info("added " + items.size() +" stream entries from: " + path);
		} catch (IOException e) {
			LOGGER.error(e);
		}
		
		
	}
	
	/**
	 * check if a file is a valid file (is video and file)
	 * @param file the actual file
	 * @return true if it's valid, else false
	 */
	private boolean isValidFile(File file) {
		try {
			String mimeType = Files.probeContentType(file.toPath());
			if (file.isFile() && mimeType != null && (mimeType.startsWith("video") || mimeType.contains("matroska"))) {
				return true;
			} else {
//				System.out.println(file.getPath() + " mime type: " + mimeType);
			}
		} catch (IOException e) {
			LOGGER.error(e);
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param file the root file you want to check
	 * @return true if it's a valid series root, else false
	 */
	private boolean isValidSeriesRoot(File file) {
		if(file.isDirectory()) {
			for (File season : file.listFiles()) {
				if (season.isDirectory()) {
					return true;
				} else {
					return false; // the root directory not only folders
				}
			}
		}
		
		return false;
	}

	// removes the ending
	private String cutOffEnd(String str) {
		if (str == null) return null;
		int pos = str.lastIndexOf(".");
		if (pos == -1) return str;
		return str.substring(0, pos);
	}
	
}
