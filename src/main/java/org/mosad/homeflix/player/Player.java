/**
 * Project-HomeFlix
 *
 * Copyright 2016-2022 <seil0@mosad.xyz>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

package org.mosad.homeflix.player;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mosad.homeflix.application.Main;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Player {
	
	private PlayerController playerController;
	private Stage stage;
	private AnchorPane pane;
	private Scene scene;
	private static final Logger LOGGER = LogManager.getLogger(Player.class.getName());
	
	// TODO move the players choose logic to a separate static class
	
	/**
	 * generate a new PlayerWindow
	 * @param currentTableFilm	the currently selected film
	 */
	public Player(String streamURL) {
		try {
			defaultPlayer(streamURL);
		} catch (Exception e) {
			LOGGER.error("Error while playing media", e);
			legacyPlayer(streamURL);
		}
	}
	
	/**
	 * start the integrated player
	 * @param streamURL 
	 */
	private void defaultPlayer(String mediaURL) {
		playerController = new PlayerController(this, mediaURL);
		
		try {
			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(getClass().getResource("/fxml/PlayerWindow.fxml"));
			fxmlLoader.setController(playerController);
			pane = (AnchorPane) fxmlLoader.load();
			stage = new Stage();
			scene = new Scene(pane);
			scene.getStylesheets().add(getClass().getResource("/css/Player.css").toExternalForm());
			stage.setScene(scene);
			stage.setTitle("HomeFlix");
			stage.getIcons().add(new Image(Main.class.getResourceAsStream("/icons/Homeflix_Icon_64x64.png")));
			stage.setOnCloseRequest(event -> {
				playerController.stop();
				stage.close();
			});
			//stage.setFullScreen(true);
			stage.show();
			
			playerController.init();
			playerController.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 */
	private void legacyPlayer(String streamURL) {
		LOGGER.warn("using fallback player!");
		if (System.getProperty("os.name").contains("Linux")) {
			String line;
			String output = "";
			Process p;
			try {
				p = Runtime.getRuntime().exec("which vlc");
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				while ((line = input.readLine()) != null) {
					output = line;
				}
				LOGGER.info("which vlc: " + output);
				input.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			if (output.contains("which: no vlc") || output == "") {
//				JFXInfoAlert vlcInfoAlert = new JFXInfoAlert("Info",
//						XMLController.getLocalBundle().getString("vlcNotInstalled"), btnStyle, primaryStage);
//				vlcInfoAlert.showAndWait();
			} else {
				try {
					new ProcessBuilder("vlc", streamURL).start();
				} catch (IOException e) {
					LOGGER.warn("An error has occurred while opening the file!", e);
				}
			}
			
		} else if (System.getProperty("os.name").contains("Windows") || System.getProperty("os.name").contains("Mac OS X")) {
			try {
				Desktop.getDesktop().open(new File(streamURL));
			} catch (IOException e) {
				LOGGER.warn("An error has occurred while opening the file!", e);
			}
		} else {
			LOGGER.error(System.getProperty("os.name") + ", OS is not supported, please contact a developer! ");
		}
	}
	
	/**
	 * check if a film is supported by the HomeFlixPlayer or not this is the case if
	 * the mime type is mp4
	 * 
	 * @param streamURL URL of the stream you want to check
	 * @return true if so, false if not
	 */
	public static boolean isSupportedFormat(String streamURL) {
		String mimeType = URLConnection.guessContentTypeFromName(streamURL);
		return mimeType != null && (mimeType.contains("mp4") || mimeType.contains("vp6"));
	}

	public Stage getStage() {
		return stage;
	}
	
	public Scene getScene() {
		return scene;
	}
	
	public Pane getPane() {
		return pane;
	}
	
}
