/**
 * Project-HomeFlix
 *
 * Copyright 2016-2022 <seil0@mosad.xyz>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

package org.mosad.homeflix.player;

import uk.co.caprica.vlcj.media.MediaRef;
import uk.co.caprica.vlcj.media.TrackType;
import uk.co.caprica.vlcj.player.base.MediaPlayer;
import uk.co.caprica.vlcj.player.base.MediaPlayerEventListener;

public class HFMediaPlayerEventListener  implements MediaPlayerEventListener {

	@Override
	public void mediaChanged(MediaPlayer mediaPlayer, MediaRef media) {
		// Auto-generated method stub
	}

	@Override
	public void opening(MediaPlayer mediaPlayer) {
		// Auto-generated method stub
	}

	@Override
	public void buffering(MediaPlayer mediaPlayer, float newCache) {
		// Auto-generated method stub
	}

	@Override
	public void playing(MediaPlayer mediaPlayer) {
		// Auto-generated method stub
	}

	@Override
	public void paused(MediaPlayer mediaPlayer) {
		// Auto-generated method stub
	}

	@Override
	public void stopped(MediaPlayer mediaPlayer) {
		// Auto-generated method stub
	}

	@Override
	public void forward(MediaPlayer mediaPlayer) {
		// Auto-generated method stub
	}

	@Override
	public void backward(MediaPlayer mediaPlayer) {
		// Auto-generated method stub
	}

	@Override
	public void finished(MediaPlayer mediaPlayer) {
		// Auto-generated method stub
	}

	@Override
	public void timeChanged(MediaPlayer mediaPlayer, long newTime) {
		// Auto-generated method stub
	}

	@Override
	public void positionChanged(MediaPlayer mediaPlayer, float newPosition) {
		// Auto-generated method stub
	}

	@Override
	public void seekableChanged(MediaPlayer mediaPlayer, int newSeekable) {
		// Auto-generated method stub
	}

	@Override
	public void pausableChanged(MediaPlayer mediaPlayer, int newPausable) {
		// Auto-generated method stub
	}

	@Override
	public void titleChanged(MediaPlayer mediaPlayer, int newTitle) {
		// Auto-generated method stub
	}

	@Override
	public void snapshotTaken(MediaPlayer mediaPlayer, String filename) {
		// Auto-generated method stub
	}

	@Override
	public void lengthChanged(MediaPlayer mediaPlayer, long newLength) {
		// Auto-generated method stub
	}

	@Override
	public void videoOutput(MediaPlayer mediaPlayer, int newCount) {
		// Auto-generated method stub
	}

	@Override
	public void scrambledChanged(MediaPlayer mediaPlayer, int newScrambled) {
		// Auto-generated method stub
	}

	@Override
	public void elementaryStreamAdded(MediaPlayer mediaPlayer, TrackType type, int id) {
		// Auto-generated method stub
	}

	@Override
	public void elementaryStreamDeleted(MediaPlayer mediaPlayer, TrackType type, int id) {
		// Auto-generated method stub
	}

	@Override
	public void elementaryStreamSelected(MediaPlayer mediaPlayer, TrackType type, int id) {
		// Auto-generated method stub
	}

	@Override
	public void corked(MediaPlayer mediaPlayer, boolean corked) {
		// Auto-generated method stub
	}

	@Override
	public void muted(MediaPlayer mediaPlayer, boolean muted) {
		// Auto-generated method stub
	}

	@Override
	public void volumeChanged(MediaPlayer mediaPlayer, float volume) {
		// Auto-generated method stub
	}

	@Override
	public void audioDeviceChanged(MediaPlayer mediaPlayer, String audioDevice) {
		// Auto-generated method stub
	}

	@Override
	public void chapterChanged(MediaPlayer mediaPlayer, int newChapter) {
		// Auto-generated method stub
	}

	@Override
	public void error(MediaPlayer mediaPlayer) {
		// Auto-generated method stub
	}

	@Override
	public void mediaPlayerReady(MediaPlayer mediaPlayer) {
		// Auto-generated method stub
	}
	
}
