[![Build Status](https://ci.mosad.xyz/api/badges/Seil0/Project-HomeFlix/status.svg)](https://ci.mosad.xyz/Seil0/Project-HomeFlix) [![Latest](https://img.shields.io/badge/Download-latest-brightgreen.svg)](https://git.mosad.xyz/Seil0/Project-HomeFlix/releases)
[![Release](https://img.shields.io/badge/dynamic/json.svg?label=release&url=https://git.mosad.xyz/api/v1/repos/Seil0/Project-HomeFlix/releases&query=$[0].tag_name)](https://git.mosad.xyz/Seil0/Project-HomeFlix/releases)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# Project-HomeFlix
Project-HomeFlix is a Kellerkinder Project, that alowes you to sort all your local saved movies in clean UI. Project-HomeFlix is free and open-source software and uses other open-source projects to provied a nice user experience.

## Installation
Simply download the Project-HomeFlix.jar from [releases](https://git.mosad.xyz/Seil0/Project-HomeFlix/releases), make sure you have the latest version of java 8 oracle jre/jdk installed, open the .jar file. If you need additional information pleas visit our [wiki](https://git.mosad.xyz/Seil0/Project-HomeFlix/wiki).

## Development information
The dev branch is **only merged** into master when a **new release** is released, so **master contains the latest  released version**. Please commit all changes to [dev](https://git.mosad.xyz/Seil0/Project-HomeFlix/src/branch/dev).

[Libraries used in this Project](https://git.mosad.xyz/Seil0/Project-HomeFlix/wiki/Documentation#used-libraries-and-apis)

## Screenshots
![Screenshot](https://raw.githubusercontent.com/Seil0/Seil0.github.io/master/images/Project-HomeFlix_MainWindow.webp)

Project-HomeFlix © 2016-2019 mosad www.mosad.xyz, Project by [@Seil0](https://git.mosad.xyz/Seil0) and [@localhorst](https://git.mosad.xyz/localhorst) 
